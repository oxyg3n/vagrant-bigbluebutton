#!/bin/bash
#
# install-bbb.sh
# 
# This script install BigBlueButton 0.9

# By storing the date now, we can calculate the duration of provisioning at the
# end of this script.

# http://docs.bigbluebutton.org/install/install.html

sed -i "deb http://archive.ubuntu.com/ubuntu trusty multiverse" /etc/apt/sources.list
sudo apt-get update -y
sudo apt-get dist-upgrade -y
sudo apt-get install software-properties-common -y
sudo add-apt-repository ppa:libreoffice/libreoffice-4-4
wget http://ubuntu.bigbluebutton.org/bigbluebutton.asc -O- | sudo apt-key add -
echo "deb http://ubuntu.bigbluebutton.org/trusty-090/ bigbluebutton-trusty main" | sudo tee /etc/apt/sources.list.d/bigbluebutton.list
sudo apt-get update


sudo apt-get install build-essential git-core checkinstall yasm texi2html libvorbis-dev libx11-dev libvpx-dev libxfixes-dev zlib1g-dev pkg-config netcat libncurses5-dev -y
FFMPEG_VERSION=2.3.3

cd /usr/local/src
if [ ! -d "/usr/local/src/ffmpeg-${FFMPEG_VERSION}" ]; then
  sudo wget "http://ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.bz2"
  sudo tar -xjf "ffmpeg-${FFMPEG_VERSION}.tar.bz2"
fi

cd "ffmpeg-${FFMPEG_VERSION}"
sudo ./configure --enable-version3 --enable-postproc --enable-libvorbis --enable-libvpx
sudo make
sudo checkinstall --pkgname=ffmpeg --pkgversion="5:${FFMPEG_VERSION}" --backup=no --deldoc=yes --default

sudo apt-get update -y
sudo apt-get install bigbluebutton -y
sudo apt-get install bbb-demo -y
sudo bbb-conf --enablewebrtc
sudo bbb-conf --clean
sudo bbb-conf --setip bbb.dev
echo "127.0.0.1 bbb.dev" >> /etc/hosts
sudo service nginx restart

echo "Big Blue button successfully installed, access via ip: http://192.168.50.7 or add rule to hosts"